package restarter

import (
	"net/http"
	"time"
)

type Restarter struct {
	Running             bool
	StopChannel         chan bool
	StartChannel        chan bool
	HealthUpdateChannel chan bool
}

func (r *Restarter) Init() {
	r.Running = false
	r.StopChannel = make(chan bool, 100)
	r.StartChannel = make(chan bool, 100)
	r.HealthUpdateChannel = make(chan bool, 100)
	r.HealthListener()
}

func (r *Restarter) HealthListener() {
	go func() {
		for {
			select {
			case _ = <-r.HealthUpdateChannel:
				go SendHealth()
			case _ = <-r.StopChannel:
				r.Running = false
			case _ = <-r.StartChannel:
				r.Running = true
			default:
				if !r.Running {
					go SendHealth()
				}
				time.Sleep(500 * time.Millisecond)
			}
		}
	}()
}

func SendHealth() {
	url := "http://localhost:8087/healthy"

	req, e := http.NewRequest("GET", url, nil)
	if e != nil {
		return
	}
	res, e2 := http.DefaultClient.Do(req)
	if e2 != nil {
		return
	}
	defer res.Body.Close()
}
