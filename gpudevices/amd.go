package gpudevices

import (
	"fmt"
	"log"
	"os/exec"
)

// Amd is a struct describing Amd card installed in a rig
type Amd struct {
	*GPUDeviceCommon
}

func (a *Amd) GetUid() int {
	return a.Uid
}

func (a *Amd) GetDeviceCommon() *GPUDeviceCommon {
	return a.GPUDeviceCommon
}

func (a *Amd) SetFanSpeed(speed int) error {
	// ohgodatool -i 3 --set-fanspeed 65

	log.Printf("[Amd] Setting Fan speed to: %d on card %d\n", speed, a.Uid)
	command := fmt.Sprintf("ohgodatool -i %d --set-fanspeed %d", a.Uid, speed)

	cmd := exec.Command("bash", "-c", command)
	err := cmd.Run()
	if err != nil {
		log.Println(err)
		return err
	}
	a.FanSpeed = speed

	return nil
}

func (a *Amd) SetPowerLimit(limit int) error {

	log.Printf("Setting Power Limit to: %d on card %d\n", limit, a.Uid)
	// ohgodatool -i 3 --set-max-power 100
	command := fmt.Sprintf("ohgodatool -i %d --set-max-power %d", a.Uid, limit)

	cmd := exec.Command("bash", "-c", command)
	err := cmd.Run()
	if err != nil {
		log.Println(err)
		return err
	}

	a.PowerLimit = limit

	a.SetFanSpeed(a.FanSpeed)
	return nil
}

func (a *Amd) SetMemTROffset(offset int) error {

	log.Printf("Setting Memory Offset  to: %d on card %d\n", offset, a.Uid)
	// ohgodatool -i 3 --mem-state 2 --mem-clock 1900

	command := fmt.Sprintf("ohgodatool -i %d --mem-state 2 --mem-clock %d", a.Uid, offset)

	cmd := exec.Command("bash", "-c", command)
	err := cmd.Run()
	if err != nil {
		log.Println(err)
		return err
	}

	a.MemOffset = offset

	a.SetFanSpeed(a.FanSpeed)

	return nil
}

func (a *Amd) SetVoltage(volts int) error {

	log.Printf("Setting Voltage  to: %d on card %d\n", volts, a.Uid)
	// ohgodatool -i 0  --core-state 7 --core-vddc-idx 11

	command := fmt.Sprintf("ohgodatool -i %d --core-state 7 --core-vddc-idx %d", a.Uid, volts)

	cmd := exec.Command("bash", "-c", command)
	err := cmd.Run()
	if err != nil {
		log.Println(err)
		return err
	}

	a.Volts = volts

	a.SetFanSpeed(a.FanSpeed)

	return nil
}

func (a *Amd) GetTemp() string {
	// ohgodatool --show-temp -i 2
	command := fmt.Sprintf("ohgodatool --show-temp -i %d", a.Uid)
	cmd := exec.Command("/bin/bash", "-c", command)
	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Println(err)
		return ""
	}
	temp := string(out[:])
	log.Println("Temp: AMD GPU ", a.Uid, temp)

	return temp
}
