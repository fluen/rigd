package gpudevices

// GPUDeviceCommon is a shared struct on which other, more specific structs are based.
// All in all, every GPU device is the same, right? :-)
type GPUDeviceCommon struct {
	Driver     string
	Pci        string
	Uid        int
	Model      string
	FanSpeed   int
	PowerLimit int
	MemOffset  int
	Volts      int
}

// GPUDevice is an interface used by a Rig to organise different types of GPUDevices
type GPUDevice interface {
	SetFanSpeed(int) error
	SetPowerLimit(int) error
	SetMemTROffset(int) error
	SetVoltage(int) error
	GetUid() int
	GetTemp() string
	GetDeviceCommon() *GPUDeviceCommon
}
