package gpudevices

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"os/exec"
	"strconv"
	"strings"

	nvidiasmi "github.com/rai-project/nvidia-smi"
)

// Nvidia is a struct describing Nvidia card installed in a rig
type Nvidia struct {
	*GPUDeviceCommon
	minorNumber int
}

func NewNvidia(dev *GPUDeviceCommon) *Nvidia {
	mn, err := getMinorNumber(dev.Pci)
	if err != nil {
		log.Println("NewNvidia() error: ", err)
		return nil
	}
	n := &Nvidia{
		GPUDeviceCommon: dev,
		minorNumber:     mn,
	}
	return n
}

func getMinorNumber(pci string) (int, error) {
	info, err := nvidiasmi.New()
	if err != nil {
		log.Println(err)
		return -1, err
	}

	for _, c := range info.GPUS {
		busID := strings.ToLower(c.PciBusID)
		if strings.Replace(busID, "0", "", -1) == strings.Replace(strings.ToLower(pci), "0", "", -1) {
			mn, err := strconv.ParseInt(c.MinorNumber, 10, 0)
			return int(mn), err
		}
	}

	return -1, errors.New("No matching PCI")

}

func (n *Nvidia) GetUid() int {
	return n.Uid
}

func (n *Nvidia) GetDeviceCommon() *GPUDeviceCommon {
	return n.GPUDeviceCommon
}

func (n *Nvidia) SetFanSpeed(speed int) error {
	log.Printf("[Nvidia] Setting Fan speed to: %d on card %d\n", speed, n.minorNumber)

	// nvidia-settings -c :0 -a [gpu:$i]/GPUFanControlState=1 -a [fan:$i]/GPUTargetFanSpeed=65;
	gpu := fmt.Sprintf("[gpu:%d]/GPUFanControlState=1", n.minorNumber)
	fan := fmt.Sprintf("[fan:%d]/GPUTargetFanSpeed=%d", n.minorNumber, speed)
	cmd := exec.Command("nvidia-settings", "-c", ":0", "-a", gpu, "-a", fan)

	var outb, errb bytes.Buffer
	cmd.Stdout = &outb
	cmd.Stderr = &errb

	err := cmd.Run()

	if err != nil {
		log.Println(err)
		return err
	}

	n.FanSpeed = speed
	return nil
}

func (n *Nvidia) SetPowerLimit(limit int) error {

	log.Printf("Setting Power Limit to: %d on card %d\n", limit, n.Uid)

	// nvidia-smi -i 0 -pl 180
	// nvidia-smi -i 0 -pm 1

	cmd := exec.Command("nvidia-smi", "-i", strconv.Itoa(n.minorNumber), "-pm", "1")
	err := cmd.Run()
	if err != nil {
		log.Println(err)
		return err
	}

	cmd = exec.Command("nvidia-smi", "-i", strconv.Itoa(n.minorNumber), "-pl", strconv.Itoa(limit))
	err = cmd.Run()

	if err != nil {
		log.Println(err)
		return err
	}

	n.PowerLimit = limit
	return nil
}

func (n *Nvidia) SetMemTROffset(offset int) error {

	log.Printf("Setting Memory Offset  to: %d on card %d\n", offset, n.Uid)

	// nvidia-settings -c :0 -a [gpu:0]/GPUMemoryTransferRateOffset[3]=2000
	o := fmt.Sprintf("nvidia-settings -c :0 -a [gpu:%d]/GPUMemoryTransferRateOffset[3]=%d", n.minorNumber, offset)

	cmd := exec.Command("bash", "-c", o)

	var outb, errb bytes.Buffer
	cmd.Stdout = &outb
	cmd.Stderr = &errb

	err := cmd.Run()
	fmt.Println("out:", outb.String(), "err:", errb.String())

	if err != nil {
		log.Println(err)
		return err
	}

	n.MemOffset = offset

	return nil
}

func (n *Nvidia) SetVoltage(volts int) error {
	return errors.New("Undervolting not supported on Nvidia cards")
}

func (n *Nvidia) GetTemp() string {
	// nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader -i 1
	command := fmt.Sprintf("nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader -i %d", n.minorNumber)
	cmd := exec.Command("/bin/bash", "-c", command)
	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Println(err)
		return ""
	}
	temp := string(out[:])
	log.Println("Temp: Nvidia Gpu ", n.minorNumber, temp)
	return temp
}
