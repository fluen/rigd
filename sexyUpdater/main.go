package main

import (
	"log"
	"net/http"

	"encoding/json"
	"fmt"
	"github.com/go-zoo/bone"
	"gitlab.com/fluen/rigd/sexyUpdater/Updater"
	"io/ioutil"
	"os"
)

func main() {
	ExecConfig("./config.json")

	r := Updater.Restarter{}
	r.Init()

	mux := bone.New()

	mux.GetFunc("/update", Updater.UpdateHandler)
	mux.GetFunc("/restart", Updater.RestartWrapper(r.RestartChannel))
	mux.GetFunc("/healthy", Updater.HealthWrapper(r.HealthChannel))

	log.Printf(fmt.Sprintf(" Starting listening on %s:%s\n", os.Getenv("UpdateListenAddr"), os.Getenv("UpdateListenPort")))
	if err := http.ListenAndServe(fmt.Sprintf("%s:%s", os.Getenv("UpdateListenAddr"), os.Getenv("UpdateListenPort")), mux); err != nil {
		log.Println("Cannot bind to specified port. Sorry.")
	}
}

func ExecConfig(path string) {
	file, err := ioutil.ReadFile(path)
	if err != nil {
		log.Printf("Error during reading config file\n%s\n", err.Error())
		os.Exit(1)
	}

	var config Config
	err = json.Unmarshal(file, &config)
	if err != nil {
		log.Printf("Error during JSON Unmarshal\n%s\n", err.Error())
		os.Exit(1)
	}

	log.Printf(`Variables read from config file:
{
	AdminAddr: %s,
	AdminPort: %s,
	DownloadPath: %s
}`, config.AdminAddr, config.AdminPort, config.DownloadPath)

	err = os.Setenv("AdminAddr", config.AdminAddr)
	if err != nil {
		log.Printf("Error setting Env Variable\n%s\n", err.Error())
	}

	err = os.Setenv("AdminPort", config.AdminPort)
	if err != nil {
		log.Printf("Error setting Env Variable\n%s\n", err.Error())
	}

	err = os.Setenv("DownloadPath", config.DownloadPath)
	if err != nil {
		log.Printf("Error setting Env Variable\n%s\n", err.Error())
	}

	err = os.Setenv("UpdateListenAddr", config.UpdateListenAddr)
	if err != nil {
		log.Printf("Error setting Env Variable\n%s\n", err.Error())
	}

	err = os.Setenv("UpdateListenPort", config.UpdateListenPort)
	if err != nil {
		log.Printf("Error setting Env Variable\n%s\n", err.Error())
	}
}

type Config struct {
	DownloadPath     string
	AdminPort        string
	AdminAddr        string
	UpdateListenAddr string
	UpdateListenPort string
}
