package Updater

import (
	"log"
	"net/http"
	"os/exec"
	"time"
)

type Restarter struct {
	RestartChannel chan bool
	HealthChannel  chan bool
	LastHealth     time.Time
}

func (r *Restarter) Init() {
	r.RestartChannel = make(chan bool)
	r.HealthChannel = make(chan bool, 1000)
	r.LastHealth = time.Now()
	r.HealthChecker()
	r.RestartListener()
}

func (r *Restarter) RestartListener() {
	go func() {
		for {
			select {
			case _ = <-r.RestartChannel:
				log.Println("sending evil echo")
				cmd := exec.Command("/bin/bash", "-c", "echo b > /proc/sysrq-trigger")
				err := cmd.Run()
				log.Println("... sent.")
				if err != nil {
					log.Printf("Something went terribly wrong!\nSeppuku did not work correctly!\nI don't know what to do now :(\n")
				}
			}
		}
	}()
}

func (r *Restarter) HealthChecker() {
	go func() {
		for {
			select {
			case _ = <-r.HealthChannel:
				r.LastHealth = time.Now()
				log.Printf("Time updated!\n")
			default:
				if time.Since(r.LastHealth).Seconds() > 600.0 {
					r.RestartChannel <- true
				}
				time.Sleep(500 * time.Millisecond)
			}
		}
	}()
}

func RestartWrapper(RestartChannel chan bool) http.HandlerFunc {
	return func(rw http.ResponseWriter, req *http.Request) {
		log.Printf("Restart request received :)\n")
		go func() {
			exception := time.After(30 * time.Second)
			select {
			case _ = <-exception:
				RestartChannel <- true
			}
		}()
		rw.WriteHeader(200)
		rw.Write([]byte("OK"))
	}
}

func HealthWrapper(HealthChannel chan bool) http.HandlerFunc {
	return func(rw http.ResponseWriter, req *http.Request) {
		log.Printf("Health update request received :)\n")
		HealthChannel <- true
		rw.WriteHeader(200)
		rw.Write([]byte("OK"))
	}
}
