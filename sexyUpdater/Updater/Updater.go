package Updater

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"time"

	"encoding/json"

	"github.com/foolin/gosupervisor"
	"github.com/pkg/errors"
)

var fluenUpdateFile string = "Fluen_Update_"
var fluenWebFile string = "fluen_update_"
var fluenDownloadCachePath string = "/tmp"
var fluenRootPath string = "/opt/fluen" //TODO: To be defined by JarŚw

func UpdateHandler(rw http.ResponseWriter, req *http.Request) {
	currentVersion := req.Header.Get("version")
	chainVersion, err := GetChainVersion(currentVersion)

	if err != nil {
		log.Printf("Error reading latest version\nAborting\n")
		rw.WriteHeader(500)
		rw.Write([]byte("Error reading latest version"))
		return
	}

	if len(chainVersion) == 0 {
		log.Printf("Already up-to-date\nAborting\n")
		rw.WriteHeader(201)
		rw.Write([]byte("Already up-to-date"))
		return
	}

	if err := KillRigd(); err != nil {
		log.Print("Error killing Rigd\nAborting\n")
		rw.WriteHeader(500)
		rw.Write([]byte("Error killing Rigd"))
		return
	}
	defer StartRigd()

	for _, version := range chainVersion {
		log.Println(version)
		if err := DownloadPackage(version); err != nil {
			log.Print("Error downloading update file\nAborting\n")
			rw.WriteHeader(500)
			rw.Write([]byte("Error downloading update file"))
			return
		}

		if err := UpdateRigd(version); err != nil {
			log.Printf("Error executing Fluen Update\nAborting\n")
			rw.WriteHeader(500)
			rw.Write([]byte("Error updating fluen"))
			return
		}
	}

	rw.WriteHeader(200)
	rw.Write([]byte("OK"))
}

func GetChainVersion(currentVersion string) ([]int, error) {
	client := http.Client{Timeout: time.Duration(9999 * time.Millisecond)}
	resp, err := client.Get(fmt.Sprintf("%s:%s/api/sex/chainversion?version=%s", os.Getenv("AdminAddr"), os.Getenv("AdminPort"), currentVersion))
	if err != nil {
		log.Printf("Error connecting to Admin\n%s\n", err.Error())
		return nil, errors.New(fmt.Sprintf("Error connecting to Admin\n%s\n", err.Error()))
	}
	defer resp.Body.Close()

	status := resp.StatusCode
	if status != 200 {
		log.Printf("Received error status code\n")
		return nil, errors.New(fmt.Sprintf("Received error status code\n"))
	}

	decoder := json.NewDecoder(resp.Body)
	chain := []int{}
	err = decoder.Decode(&chain)

	if err != nil {
		log.Printf("Error reading response\n%s\n", err.Error())
		return nil, errors.New(fmt.Sprintf("Error reading response\n%s\n", err.Error()))
	}

	return chain, nil
}

func DownloadPackage(version int) error {
	file, err := os.Create(fmt.Sprintf("%s/%s%d.tar.gz", fluenDownloadCachePath, fluenUpdateFile, version))
	defer file.Close()

	if err != nil {
		log.Printf("Error creating update file\n%s\n", err.Error())
		return errors.New(fmt.Sprintf("Error creating update file\n%s\n", err.Error()))
	}
	defer file.Close()

	toDownload := fmt.Sprintf("%s/%s%d.tar.gz", os.Getenv("DownloadPath"), fluenWebFile, version)
	log.Println("I'm about to download: ", toDownload)
	resp, err := http.Get(toDownload)
	if err != nil {
		log.Printf("Error downloading update file\n%s\n", err.Error())
		return errors.New(fmt.Sprintf("Error downloading update file\n%s\n", err.Error()))
	}
	if resp.StatusCode != 200 {
		log.Printf("Error downloading update file\n")
		return errors.New(fmt.Sprintf("Error downloading update file\n%s\n", resp.Status))
	}
	defer resp.Body.Close()

	_, err = io.Copy(file, resp.Body)
	if err != nil {
		log.Printf("Error saving update file\n%s\n", err.Error())
		return errors.New(fmt.Sprintf("Error saving update file\n%s\n", err.Error()))
	}

	log.Printf("Update file saved successfuly\n")
	return nil
}

func KillRigd() error {
	rpc := gosupervisor.New("http://localhost:9001/RPC2")

	_, err := rpc.StopProcess("rigd", true)
	if err != nil {
		log.Printf("Error stopping Rigd\n%s\n", err.Error())
		return errors.New(fmt.Sprintf("Error stopping Rigd\n%s\n", err.Error()))
	}

	log.Printf("Rigd kill successful\n")
	return nil
}

func WriteLogFile(version int, msg string) {

	logFile, logErr := os.Create(fmt.Sprintf("%s/Update_Log_%d.log", fluenRootPath, version))
	if logErr != nil {
		log.Printf("Error creating Log File\n%s\n", logErr.Error())
		return
	}
	defer logFile.Close()

	logFile.WriteString(msg)
	logFile.Sync()
}

func UpdateRigd(version int) error {
	archive := fmt.Sprintf("%s/%s%d.tar.gz", fluenDownloadCachePath, fluenUpdateFile, version)
	log.Println(archive)
	cmd := exec.Command("tar", "-zxvf", archive, "-C", fluenRootPath)
	err := cmd.Run()
	if err != nil {
		log.Printf("Error extracting update package - %s\n", err.Error())
		return errors.New(fmt.Sprintf("Error extracting update package - %s\n", err.Error()))
	}

	cmd = exec.Command("chmod", "755", fmt.Sprintf("%s/%s%d/update", fluenRootPath, fluenUpdateFile, version))
	cmd.Dir = fmt.Sprintf("%s/%s%d", fluenRootPath, fluenUpdateFile, version)
	if e := cmd.Run(); e != nil {
		log.Println("chmod 755 update didn't work.", e)
		return e
	}
	cmd = exec.Command(fmt.Sprintf("%s/%s%d/update", fluenRootPath, fluenUpdateFile, version))
	updateOutput, err := cmd.CombinedOutput()

	WriteLogFile(version, string(updateOutput[:]))
	if err != nil {
		log.Printf("Error updating Fluen - %s\n", err.Error())
		return errors.New(fmt.Sprintf("Error updating Fluen - %s\n", err.Error()))
	}

	err = os.RemoveAll(fmt.Sprintf("%s/%s%d", fluenRootPath, fluenUpdateFile, version))
	if err != nil {
		log.Printf("Error during cleanup\nSkipping - %s\n", err.Error())
	}

	err = os.RemoveAll(fmt.Sprintf("%s/%s%d.tar.gz", fluenDownloadCachePath, fluenUpdateFile, version))
	if err != nil {
		log.Printf("Error during cleanup\nSkipping - %s\n", err.Error())
	}

	return nil
}

func StartRigd() error {
	rpc := gosupervisor.New("http://localhost:9001/RPC2")

	_, err := rpc.StartProcess("rigd", true)
	if err != nil {
		log.Printf("Error starting Rigd\n%s\n", err.Error())
		return errors.New(fmt.Sprintf("Error starting Rigd\n%s\n", err.Error()))
	}

	log.Printf("Rigd start successful\n")
	return nil
}
