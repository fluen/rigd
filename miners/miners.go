package miners

type Miner interface {
	RunInShell(string, string) error
	Stop()
	PublishOutput(string)
	ReadStats(string)
	SetOutput(bool)
}
