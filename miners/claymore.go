package miners

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"gitlab.com/fluen/rigd/gpudevices"
	"gitlab.com/fluen/rigd/restarter"
	"gitlab.com/fluen/rigd/sexyServer/server"
)

type Claymore struct {
	Wallet               string
	Pool                 string
	DockerImage          string
	outputScanner        *bufio.Scanner
	quitPublishingOutput chan bool
	readStatsChan        chan bool
	minerCmd             *exec.Cmd
	Output               bool
	sexyServer           string
	gpuSwapSource        chan map[string]string
	GPUs                 []gpudevices.GPUDevice
	Restart              *restarter.Restarter
	// TODO: THis is to be changed. For now, dirty hack. ble.
	Email    string
	FluenKey string
}

func NewClaymore(wallet string, pool string, sexyServer string, GPUs []gpudevices.GPUDevice, email string, fluenKey string, restarter *restarter.Restarter) *Claymore {
	c := &Claymore{
		DockerImage:   "fluen-claymore",
		Wallet:        wallet,
		Pool:          pool,
		sexyServer:    sexyServer,
		gpuSwapSource: make(chan map[string]string),
		GPUs:          GPUs,
		Email:         email,
		FluenKey:      fluenKey,
		Restart:       restarter,
	}

	c.quitPublishingOutput = make(chan bool)
	c.readStatsChan = make(chan bool)

	return c
}

func (c *Claymore) RunContainer() error {
	cli, err := client.NewEnvClient()
	if err != nil {
		return err
	}

	ctx := context.Background()

	resp, err := cli.ContainerCreate(ctx, &container.Config{
		Image: "fluen-claymore",
		Tty:   true,
	}, nil, nil, "")

	if err != nil {
		panic(err)
	}

	if err := cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		panic(err)
	}

	return nil

}

func (c *Claymore) RunInShell(rigId string, rigName string) error {
	c.Restart.StartChannel <- true

	// ./ethdcrminer64 -epool eu1.ethermine.org:4444  -ewal 0xE7007C8971B3E9D3869c03F7215B1B9B495c42a4.szesckart -epsw x -mode 1 -ftime 10 -colors 0
	//cs := "/root/claymore/ethdcrminer64 -epool eu1.ethermine.org:4444  -ewal 0xE7007C8971B3E9D3869c03F7215B1B9B495c42a4.szesckart -epsw x -mode 1 -ftime 10 -colors 0"
	log.Printf("---------------------Current rig name is: %s", rigName)
	cs := fmt.Sprintf("/root/claymore/ethdcrminer64 -epool %s -ewal %s.%s -epsw x -mode 1 -ftime 10 -colors 0", c.Pool, c.Wallet, rigName)

	c.minerCmd = exec.Command("bash", "-c", cs)
	cmdReader, err := c.minerCmd.StdoutPipe()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error creating StdoutPipe for Cmd", err)
		return err
	}

	c.outputScanner = bufio.NewScanner(cmdReader)

	err = c.minerCmd.Start()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error starting Cmd", err)
		return err
	}

	c.PublishOutput(rigId)

	err = c.minerCmd.Wait()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error waiting for Cmd", err)
		return err
	}

	return nil
}

func (c *Claymore) Stop() {
	c.Restart.StopChannel <- true

	select {
	case c.readStatsChan <- true:
	case <-time.After(time.Second):
		return
	}

	select {
	case c.quitPublishingOutput <- true:
	case <-time.After(time.Second):
		return
	}

	err := c.minerCmd.Process.Kill()
	if err != nil {
		log.Println("Cannot stop Claymore? Um... ", err)
	}
}

func (c *Claymore) SetOutput(o bool) {
	c.Output = o
}

func (c *Claymore) PublishOutput(rigId string) {
	// /api/write-output
	go func() {
		output := c.gpuSwapSource
		url := fmt.Sprintf("%s/api/write-output", c.sexyServer)
		client := &http.Client{}
		input := make(chan string, 100)
		control := make(chan bool)
		b := true
		determine := &b
		go func() {
			*determine = <-control
		}()
		DetermineCards(input, output, control)
		for {
			select {
			case <-c.quitPublishingOutput:
				return
			default:
				c.outputScanner.Scan()
				if *determine {
					input <- c.outputScanner.Text()
				}
				if !c.Output {
					continue
				}
				/*
					Email: "test@example.com",
					Fluen: "C0D0B8F0-5440-400A-B18D-67B40DE00E78"
				*/

				msgJSON := fmt.Sprintf(`{"RigId": "%s", "Value": "%s", "Email":"%s","Fluen":"%s"}`, rigId, c.outputScanner.Text(), c.Email, c.FluenKey)

				req, err := http.NewRequest("POST", url, bytes.NewBuffer([]byte(msgJSON)))
				req.Header.Set("Content-Type", "application/json")
				req.Header.Set("fluen-key", c.FluenKey)
				req.Header.Set("email", c.Email)
				req.Header.Set("rigid", rigId)

				resp, err := client.Do(req)
				log.Println("Writing output..." + msgJSON)
				if err != nil {
					log.Println("Cannot send output to server: ", err)
					continue
				}

				resp.Body.Close()
			}
		}
	}()

}
func (c *Claymore) ReadStats(rigId string) {
	type Res struct {
		Id     int
		Result []string
		Error  string
	}

	log.Println("ReadStats")

	gpuSwapSource := c.gpuSwapSource
	GPUs := c.GPUs

	url := fmt.Sprintf("%s/api/write-stats", c.sexyServer)
	client := &http.Client{}

	gpuMap := make(map[int]int)
	for i := 0; i < 30; i++ {
		gpuMap[i] = i
	}

	go func() {
		gpuSwap := <-gpuSwapSource
		for k, v := range gpuSwap {
			tempKey, _ := strconv.Atoi(strings.TrimSuffix(k, ":0:0"))
			for i := 0; i < len(GPUs); i++ {
				tempPci, _ := strconv.Atoi(strings.TrimSuffix(strings.TrimPrefix(GPUs[i].GetDeviceCommon().Pci, "0000:"), ":00.0"))
				if tempKey == tempPci {
					// gpuMap["system_id"] = claymore_id
					gpuMap[GPUs[i].GetDeviceCommon().Uid], _ = strconv.Atoi(v)
				}
			}
		}
	}()

	for {
		select {
		case <-c.readStatsChan:
			return
		default:
			time.Sleep(time.Second)
			conn, err := net.Dial("tcp", "localhost:3333")
			if err != nil {
				log.Println(err)
				continue
			}
			// send to socket
			fmt.Fprintf(conn, "{\"id\":0,\"jsonrpc\":\"2.0\",\"method\":\"miner_getstat1\"}\n")
			// listen for reply
			// {"id": 0, "result": ["11.7 - ETH", "1", "103606;5;0", "21096;25586;31999;24923", "0;0;0", "off;off;off;off", "54;63;60;63;45;65;38;65", "eu1.ethermine.org:4444", "0;0;0;0"], "error": null}
			conn.SetReadDeadline(time.Now().Add(time.Second))
			message, _ := bufio.NewReader(conn).ReadString('}')
			var dat Res
			if err := json.Unmarshal([]byte(message), &dat); err != nil {
				continue
			}

			mhsPerCard := make([]string, 4)
			mhsPerCard = strings.Split(dat.Result[3], ";")

			mhsTotal := make([]string, 3)
			mhsTotal = strings.Split(dat.Result[2], ";")

			total, _ := strconv.ParseFloat(mhsTotal[0], 32)

			msg := server.StatsRequest{
				RigId:   rigId,
				Overall: strconv.FormatFloat(total*0.001, 'f', -1, 32),
				Email:   c.Email,
				Fluen:   c.FluenKey,
			}

			if total != 0.0 {
				c.Restart.HealthUpdateChannel <- true
			}

			msg.PerCard = make([]server.StatsCard, len(mhsPerCard))

			for i := 0; i < len(mhsPerCard); i++ {
				floatMhs, _ := strconv.ParseFloat(mhsPerCard[gpuMap[i]], 32) // this is "correct" order (given by claymore)
				msg.PerCard[i].Mhs = string(strconv.FormatFloat(floatMhs*0.001, 'f', -1, 32))
				msg.PerCard[i].Temp = c.GPUs[i].GetTemp()
				msg.PerCard[i].Id = c.GPUs[i].GetDeviceCommon().Pci
			}

			var buf bytes.Buffer
			enc := json.NewEncoder(&buf)
			if err := enc.Encode(&msg); err != nil {
				log.Println("Cannot encode stats msg: ", err)
				continue
			}

			req, err := http.NewRequest("POST", url, bytes.NewBuffer(buf.Bytes()))
			req.Header.Set("Content-Type", "application/json")
			req.Header.Set("fluen-key", c.FluenKey)
			req.Header.Set("email", c.Email)
			req.Header.Set("rigid", rigId)

			resp, err := client.Do(req)
			if err != nil {
				log.Println("Cannot send output to server: ", err)
				continue
			}

			log.Println(msg)

			resp.Body.Close()

		}
	}
}

func DetermineCards(input chan string, output chan map[string]string, control chan bool) {
	go func() {
		internalStop := make(chan bool)
		GPUs := make(map[string]string)
		body := false
		for {
			select {
			case msg := <-input:
				if body || strings.Contains(msg, "Cards available:") {
					body = true
					if strings.HasPrefix(msg, "Total cards:") {
						control <- false
						output <- GPUs
						internalStop <- true
					}
					if strings.HasPrefix(msg, "GPU #") {
						firstSplit := strings.SplitAfter(msg, "GPU #")
						if strings.Contains(msg, "pci bus ") {
							secondSplit := strings.SplitAfter(msg, "pci bus ")
							GPUs[strings.TrimSuffix(secondSplit[1], ")")] = string(firstSplit[1][0])
						}
					}
				}

			case <-internalStop:
				break
			}
		}
	}()
}
