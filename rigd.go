package main

import (
	"crypto/md5"
	"io"
	"log"
	"os"
	"time"

	"github.com/streadway/amqp"
	"gitlab.com/fluen/rigd/rig"
)

var configFile string = "/etc/fluen/fluen.conf"

func failOnError(err error, msg string) {
	if err != nil {
		log.Println(err, msg)
		os.Exit(128)
	}
}

func configReload() {
	go func() {
		var prevHash int64 = 0
		for {
			f, err := os.Open(configFile)
			if err != nil {
				time.Sleep(time.Second)
				continue
			}
			h := md5.New()
			hash, err := io.Copy(h, f)
			if err == nil && prevHash != hash && prevHash != 0 {
				os.Exit(0)
			}
			prevHash = hash
			time.Sleep(time.Second)
			continue
		}
	}()
}

func main() {
	var config rig.Config

	// TODO: Fix this shitty code.
	for {
		config = rig.ReadConfig(configFile)
		if config.RabbitURL != "" {
			break
		}
		time.Sleep(time.Second * 5)
	}

	configReload()

	msgs, conn, ch := connectToRabbit(&config)
	defer conn.Close()
	defer ch.Close()

	forever := make(chan bool)

	r := rig.NewRig(config)
	r.SetParams()
	for {
		if e := r.SetProfile(); e == nil {
			break
		}
		time.Sleep(time.Second * 5)
	}
	r.StartClaymore()
	r.PingAlive()

	go func(r *rig.Rig) {
		for d := range msgs {
			log.Printf(" [x] %s", d.Body)
			r.Execute(d.Body)
		}
	}(r)

	log.Printf(" [*] Waiting for logs. To exit press CTRL+C")
	<-forever

}

func connectToRabbit(config *rig.Config) (<-chan amqp.Delivery, *amqp.Connection, *amqp.Channel) {

	//	conn, err := amqp.Dial("amqp://guest:guest@192.168.8.106:5672/")
	var conn *amqp.Connection
	var err error
	counter := 0
	for {
		conn, err = amqp.Dial(config.RabbitURL)
		if err == nil {
			break
		}
		if counter == 10 {
			log.Println("Cannot connect to RabbitMQ.", err)
			os.Exit(0)
		}

		time.Sleep(time.Second * 3)
		counter++

	}
	exchange := config.Email

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")

	err = ch.ExchangeDeclare(
		exchange, // name
		"fanout", // type
		true,     // durable
		false,    // auto-deleted
		false,    // internal
		false,    // no-wait
		nil,      // arguments
	)
	failOnError(err, "Failed to declare an exchange")

	q, err := ch.QueueDeclare(
		"",    // name
		false, // durable
		false, // delete when unused
		true,  // exclusive
		false, // no-wait
		nil,   // arguments
	)
	failOnError(err, "Failed to declare a queue")

	err = ch.QueueBind(
		q.Name,   // queue name
		"",       // routing key
		exchange, // exchange
		false,
		nil)
	failOnError(err, "Failed to bind a queue")

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")

	return msgs, conn, ch

}
