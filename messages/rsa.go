package messages

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"log"
)

func ReadKeyFromFile() (*rsa.PrivateKey, error) {
	privStr, err := ioutil.ReadFile("/etc/fluen/key.priv")
	if err != nil {
		log.Println("Cannot read /etc/fluen/key.priv")
		return nil, err
	}

	block, _ := pem.Decode(privStr)
	privatekey, _ := x509.ParsePKCS1PrivateKey(block.Bytes)

	return privatekey, nil
}

func EncryptMsg(msg []byte) ([]byte, error) {
	privatekey, e := ReadKeyFromFile()
	if e != nil {
		return nil, e
	}
	publickey := &privatekey.PublicKey

	label := []byte("")
	sha1hash := sha1.New()
	encryptedmsg, err := rsa.EncryptOAEP(sha1hash, rand.Reader, publickey, msg, label)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	return encryptedmsg, nil
}

func DecryptMsg(msg []byte) ([]byte, error) {

	sha1hash := sha1.New()
	label := []byte("")

	privatekey, e := ReadKeyFromFile()
	if e != nil {
		return nil, e
	}

	decryptedmsg, err := rsa.DecryptOAEP(sha1hash, rand.Reader, privatekey, msg, label)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	return decryptedmsg, nil
}
