package messages

// RigMsg is part of Fluen API used by Rig.RegisterRig()
type RigMsg struct {
	Ident      string
	Name       string
	GPUDevices []GPUDeviceMsg
	PubKey     string
}

// GPUDeviceMsg is part of Fluen API used by RigMsg
type GPUDeviceMsg struct {
	Pci    string
	Model  string
	Uid    int
	Driver string
}

// GenericControlMsg is part of Fluen API
type GenericControlMsg struct {
	Type  string
	Value string
	RigId string
	Pci   string
}
