package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/fluen/rigd/rig"
	"gopkg.in/urfave/cli.v1"
)

type flags struct {
	FanSpeed   int
	PowerLimit int
	MemOffset  int
	CardPci    string
	Wallet     string
	Pool       string
	RigName    string
}

func main() {

	config := rig.ReadConfig("/etc/fluen/fluen.conf")

	params := &flags{}

	app := cli.NewApp()
	app.Name = "Fluen"
	app.Version = "0.0.1"
	app.Compiled = time.Now()
	app.Authors = []cli.Author{
		cli.Author{
			Name:  "FluenOS",
			Email: "fluen@fluen.org",
		},
	}
	app.Usage = "command line client to manage your rig"
	//app.UsageText = "contrive - demonstrating the available API"
	//app.ArgsUsage = "[args and such]"
	app.HelpName = "fluen"

	app.Commands = []cli.Command{
		cli.Command{
			Name:        "fan-speed",
			Aliases:     []string{"fs"},
			Usage:       "set fan speed for a card",
			UsageText:   "",
			Description: "set fan speed for a card on your rig",
			ArgsUsage:   "[speed]",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:        "cardPci, i",
					Usage:       "specify card id",
					Value:       "-1",
					Destination: &params.CardPci,
				},
				cli.IntFlag{
					Name:        "speed, s",
					Usage:       "specify fan speed",
					Value:       0,
					Destination: &params.FanSpeed,
				},
			},
			Action: func(c *cli.Context) error {
				rig := rig.NewRig(config)
				if err := rig.SetFanSpeed(params.CardPci, params.FanSpeed); err != nil {
					fmt.Println("error setting fan speed: ", err)
				}
				return nil
			},
		},

		cli.Command{
			Name:        "power-limit",
			Aliases:     []string{"pl"},
			Usage:       "set power limit for a card",
			UsageText:   "",
			Description: "set power limit for a card on your rig",
			ArgsUsage:   "[limit]",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:        "cardPci, i",
					Usage:       "specify card id",
					Value:       "-1",
					Destination: &params.CardPci,
				},
				cli.IntFlag{
					Name:        "limit, l",
					Usage:       "specify power limit",
					Value:       0,
					Destination: &params.PowerLimit,
				},
			},
			Action: func(c *cli.Context) error {
				rig := rig.NewRig(config)
				if err := rig.SetPowerLimit(params.CardPci, params.PowerLimit); err != nil {
					fmt.Println("error setting power limit: ", err)
				}
				return nil
			},
		},

		cli.Command{
			Name:        "mem-offset",
			Aliases:     []string{"mo"},
			Usage:       "set memory transfer rate offset for a card",
			UsageText:   "",
			Description: "set memory transfer rate offset  for a card on your rig",
			ArgsUsage:   "[offset]",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:        "cardId, i",
					Usage:       "specify card id",
					Value:       "-1",
					Destination: &params.CardPci,
				},
				cli.IntFlag{
					Name:        "offset, o",
					Usage:       "specify memory transfer rate offset",
					Value:       0,
					Destination: &params.MemOffset,
				},
			},
			Action: func(c *cli.Context) error {
				rig := rig.NewRig(config)
				if err := rig.SetMemOffset(params.CardPci, params.MemOffset); err != nil {
					fmt.Println("error setting memory offset: ", err)
				}
				return nil
			},
		},

		cli.Command{
			Name:        "register-rig",
			Aliases:     []string{"rr"},
			Usage:       "register rig to fluen.org account",
			UsageText:   "",
			Description: "register rig to fluen.org account",
			Action: func(c *cli.Context) error {
				config.Unregistered = true
				rig := rig.NewRig(config)
				if err := rig.RegisterRig(); err != nil {
					log.Println("Cannot register Rig: ", err)
					os.Exit(126)
				}
				return nil
			},
		},

		cli.Command{
			Name:        "run-claymore",
			Aliases:     []string{"rc"},
			Usage:       "run claymore",
			UsageText:   "",
			Description: "run claymore",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:        "wallet, w",
					Usage:       "specify wallet",
					Value:       "0xE7007C8971B3E9D3869c03F7215B1B9B495c42a4",
					Destination: &params.Wallet,
				},
				cli.StringFlag{
					Name:        "pool, p",
					Usage:       "specify pool",
					Value:       "eu1.ethermine.org:4444",
					Destination: &params.Pool,
				},
			},
			Action: func(c *cli.Context) error {
				rig := rig.NewRig(config)
				rig.SetMiner("claymore")
				err := rig.Miner.RunInShell(rig.Ident, rig.Config.RigName)
				if err != nil {
					fmt.Println(err)
					return err
				}

				return nil
			},
		},
	}

	app.Run(os.Args)

}
