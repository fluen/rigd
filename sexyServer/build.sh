#!/bin/bash

if [ -z "$GOENV" ]; then
  echo "No GOENV set";
  exit 0
fi

env GOOS=linux go build
docker build -t sexyserver -f Dockerfile.$GOENV .
docker tag sexyserver:latest 345723327866.dkr.ecr.eu-west-1.amazonaws.com/sexyserver:latest
docker push 345723327866.dkr.ecr.eu-west-1.amazonaws.com/sexyserver:latest
