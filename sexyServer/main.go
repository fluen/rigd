package main

import (
	"log"
	"net/http"

	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/go-zoo/bone"
	"gitlab.com/fluen/rigd/sexyServer/server"
)

func main() {
	fmt.Printf("___________________________________________________________________________________________________________________________________________________________________\n___d888888o.___8_8888888888___`8.`8888.______,8'_`8.`8888.______,8'_d888888o.___8_8888888888___8_888888888o._`8.`888b___________,8'_8_8888888888___8_888888888o.___\n_.`8888:'_`88._8_8888__________`8.`8888.____,8'___`8.`8888.____,8'.`8888:'_`88._8_8888_________8_8888____`88._`8.`888b_________,8'__8_8888_________8_8888____`88.__\n_8.`8888.___Y8_8_8888___________`8.`8888.__,8'_____`8.`8888.__,8'_8.`8888.___Y8_8_8888_________8_8888_____`88__`8.`888b_______,8'___8_8888_________8_8888_____`88__\n_`8.`8888._____8_8888____________`8.`8888.,8'_______`8.`8888.,8'__`8.`8888._____8_8888_________8_8888_____,88___`8.`888b_____,8'____8_8888_________8_8888_____,88__\n__`8.`8888.____8_888888888888_____`8.`88888'_________`8.`88888'____`8.`8888.____8_888888888888_8_8888.___,88'____`8.`888b___,8'_____8_888888888888_8_8888.___,88'__\n___`8.`8888.___8_8888_____________.88.`8888.__________`8._8888______`8.`8888.___8_8888_________8_888888888P'______`8.`888b_,8'______8_8888_________8_888888888P'___\n____`8.`8888.__8_8888____________.8'`8.`8888.__________`8_8888_______`8.`8888.__8_8888_________8_8888`8b___________`8.`888b8'_______8_8888_________8_8888`8b_______\n8b___`8.`8888._8_8888___________.8'__`8.`8888.__________8_8888___8b___`8.`8888._8_8888_________8_8888_`8b.__________`8.`888'________8_8888_________8_8888_`8b._____\n`8b.__;8.`8888_8_8888__________.8'____`8.`8888._________8_8888___`8b.__;8.`8888_8_8888_________8_8888___`8b._________`8.`8'_________8_8888_________8_8888___`8b.___\n_`Y8888P_,88P'_8_888888888888_.8'______`8.`8888.________8_8888____`Y8888P_,88P'_8_888888888888_8_8888_____`88.________`8.`__________8_888888888888_8_8888_____`88._\n")

	ExecConfig("./config.json")

	OutputData := server.OutputStorage{}
	StatsData := server.StatsStorage{}
	AuthData := server.AuthData{}

	outputChannel := make(chan server.OutputBundle, 1000)
	inputChannel := make(chan server.OutputRequest, 1000)
	outputStatChannel := make(chan server.StatsBundle, 1000)
	inputStatChannel := make(chan server.StatsRequest, 1000)
	authChannel := make(chan server.AuthRequest, 1000)

	OutputData.Init()
	StatsData.Init()
	AuthData.Init()

	OutputData.ReadOutput(outputChannel)
	OutputData.WriteOutput(inputChannel)

	StatsData.ReadStats(outputStatChannel)
	StatsData.WriteStats(inputStatChannel)

	AuthData.AuthMe(authChannel)

	mux := bone.New()
	mux.GetFunc("/monitor", MonitorHandler)

	mux.PostFunc("/api/write-output", server.PostWriteOutputHandler(inputChannel, authChannel))
	mux.GetFunc("/api/read-output", server.PostReadOutputHandler(outputChannel, &OutputData, authChannel))

	mux.PostFunc("/api/write-stats", server.PostWriteStatsHandler(inputStatChannel, authChannel))
	mux.PostFunc("/api/read-stats", server.PostReadStatsHandler(outputStatChannel, authChannel))

	if os.Getenv("INSECURE") == "true" {
		log.Printf(fmt.Sprintf(" Starting INSECURE listening on %s:%s\n", os.Getenv("SexListenAddr"), os.Getenv("SexListenPort")))
		if err := http.ListenAndServe(fmt.Sprintf("%s:%s", os.Getenv("SexListenAddr"), os.Getenv("SexListenPort")), mux); err != nil {
			log.Println("Cannot bind to specified port. Sorry.", err.Error())
		}
	} else {
		log.Printf(fmt.Sprintf(" Starting listening on %s:%s\n", os.Getenv("SexListenAddr"), os.Getenv("SexListenPort")))
		if err := http.ListenAndServeTLS(fmt.Sprintf("%s:%s", os.Getenv("SexListenAddr"), os.Getenv("SexListenPort")), "server.crt", "server.key", mux); err != nil {
			log.Println("Cannot bind to specified port. Sorry.")
		}
	}
}

func MonitorHandler(rw http.ResponseWriter, req *http.Request) {
	rw.Write([]byte("OK"))
}

func ExecConfig(path string) {
	file, err := ioutil.ReadFile(path)
	if err != nil {
		log.Printf("Error during reading config file\n%s\n", err.Error())
		os.Exit(1)
	}

	var config Config
	err = json.Unmarshal(file, &config)
	if err != nil {
		log.Printf("Error during JSON Unmarshal\n%s\n", err.Error())
		os.Exit(1)
	}

	log.Printf(`Variables read from config file:
{
	SexAdminAddr: %s,
	SexAdminPort: %s,
	SexListenAddr: %s,
	SexListenPort: %s
}`, config.SexAdminAddr, config.SexAdminPort, config.SexListenAddr, config.SexListenPort)

	err = os.Setenv("SexAdminAddr", config.SexAdminAddr)
	if err != nil {
		log.Printf("Error setting Env Variable\n%s\n", err.Error())
	}

	err = os.Setenv("SexAdminPort", config.SexAdminPort)
	if err != nil {
		log.Printf("Error setting Env Variable\n%s\n", err.Error())
	}

	err = os.Setenv("SexListenAddr", config.SexListenAddr)
	if err != nil {
		log.Printf("Error setting Env Variable\n%s\n", err.Error())
	}

	err = os.Setenv("SexListenPort", config.SexListenPort)
	if err != nil {
		log.Printf("Error setting Env Variable\n%s\n", err.Error())
	}

	err = os.Setenv("INSECURE", config.Insecure)
	if err != nil {
		log.Printf("Error setting Env Variable\n%s\n", err.Error())
	}

	err = os.Setenv("InfluxDbAddr", config.InfluxDbAddr)
	if err != nil {
		log.Printf("Error setting Env Variable\n%s\n", err.Error())
	}

	err = os.Setenv("InfluxDbName", config.InfluxDbName)
	if err != nil {
		log.Printf("Error setting Env Variable\n%s\n", err.Error())
	}

	err = os.Setenv("InfluxDbUser", config.InfluxDbUser)
	if err != nil {
		log.Printf("Error setting Env Variable\n%s\n", err.Error())
	}

	err = os.Setenv("InfluxDbPass", config.InfluxDbPass)
	if err != nil {
		log.Printf("Error setting Env Variable\n%s\n", err.Error())
	}
}

type Config struct {
	SexListenAddr string
	SexListenPort string
	SexAdminAddr  string
	SexAdminPort  string
	Insecure      string
	InfluxDbAddr  string
	InfluxDbName  string
	InfluxDbUser  string
	InfluxDbPass  string
}
