package server

import (
	"fmt"
	"github.com/influxdata/influxdb/client/v2"
	"log"
	"os"
	"strconv"
	"time"
)

func FeedDB(key string, overall string) {

	c, err := client.NewHTTPClient(client.HTTPConfig{
		Addr:     fmt.Sprintf("%s", os.Getenv("InfluxDbAddr")),
		Username: fmt.Sprintf("%s", os.Getenv("InfluxDbUser")),
		Password: fmt.Sprintf("%s", os.Getenv("InfluxDbPass")),
	})
	if err != nil {
		log.Printf("Error connecting to DB\n%s\n", err.Error())
		return
	}
	defer c.Close()

	bp, err := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  fmt.Sprintf("%s", os.Getenv("InfluxDbName")),
		Precision: "s",
	})
	if err != nil {
		log.Printf("Error creating batch\n%s\n", err.Error())
		return
	}

	ovrl, err := strconv.ParseFloat(overall, 64)
	if err != nil {
		log.Printf("Error converting strtofloat\n%s\n", err.Error())
		return
	}

	tags := map[string]string{}
	fields := map[string]interface{}{
		"overall": ovrl,
	}

	pt, err := client.NewPoint(fmt.Sprintf("%s_%s", "data", key), tags, fields, time.Now())
	if err != nil {
		log.Printf("Error creating new point\n%s\n", err.Error())
		return
	}
	bp.AddPoint(pt)

	// Write the batch
	if err := c.Write(bp); err != nil {
		log.Printf("Error writing batch\n%s\n", err.Error())
		return
	}

	// Close client resources
	if err := c.Close(); err != nil {
		log.Printf("Error closing client resources\n%s\n", err.Error())
		return
	}
}
