package server

import (
	"encoding/json"
	"log"
	"net/http"
)

type StatsCard struct {
	Id   string
	Mhs  string
	Temp string
}

type StatsRequest struct {
	RigId   string
	Overall string
	PerCard []StatsCard
	Email   string
	Fluen   string
}

func PostWriteStatsHandler(channel chan StatsRequest, auth chan AuthRequest) http.HandlerFunc {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		decoder := json.NewDecoder(req.Body)
		Stats := StatsRequest{}

		if err := decoder.Decode(&Stats); err != nil {
			log.Printf("Error during json Decode in Func PostWriteStatsHandler!\nError: %s\nContact Server Administrator!\n", err.Error())
			rw.WriteHeader(500)
			rw.Write([]byte("ERROR"))
			return
		}

		if !InlineAuth(Stats.Email, Stats.Fluen, auth) {
			log.Printf("User: %s authentication failed\n", Stats.Email)
			return
		}

		log.Printf("Stats received from Rig: %s\nRig mining: %s\n", Stats.RigId, Stats.Overall)
		channel <- Stats

		rw.WriteHeader(200)
		rw.Write([]byte("OK"))
		return
	})
}

func PostReadStatsHandler(channel chan StatsBundle, auth chan AuthRequest) http.HandlerFunc {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		decoder := json.NewDecoder(req.Body)
		encoder := json.NewEncoder(rw)
		Stats := StatsRequest{}

		if err := decoder.Decode(&Stats); err != nil {
			log.Printf("Error during json Decode in Func PostReadStatsHandler!\nError: %s\nContact Server Administrator!\n", err.Error())
			rw.WriteHeader(500)
			rw.Write([]byte("ERROR"))
			return
		}

		if !InlineAuth(Stats.Email, Stats.Fluen, auth) {
			log.Printf("User: %s authentication failed\n", Stats.Email)
			return
		}

		log.Printf("Request received to read Stats from Rig: %s\n", Stats.RigId)
		resp := make(chan StatsRequest)

		channel <- StatsBundle{Stat: Stats, Channel: resp}

		rw.WriteHeader(200)
		encoder.Encode(<-resp)
		return
	})
}
