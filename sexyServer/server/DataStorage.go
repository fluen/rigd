package server

import (
	"log"
	"sync"
)

//-----------------------------OUTPUT Types-----------------------------

type OutputStorage struct {
	Output         map[string][]OutputRequest
	Position       map[string]int
	ReadPosition   map[string]int
	TimeoutChannel map[string]chan float64
	SSEHandlers    map[string]*SSEHandler
	Mutex          sync.RWMutex
}

type OutputBundle struct {
	Output  OutputRequest
	Channel chan OutputRequest
}

//-----------------------------STATS Types-----------------------------

type StatsStorage struct {
	Stats            map[string]StatsRequest
	RetentionCounter map[string]int
	Mutex            sync.RWMutex
}

type StatsBundle struct {
	Stat    StatsRequest
	Channel chan StatsRequest
}

//-----------------------------OUTPUT Functions-----------------------------

func (o *OutputStorage) Init() {
	o.Output = make(map[string][]OutputRequest)
	o.Position = make(map[string]int)
	o.ReadPosition = make(map[string]int)
	o.TimeoutChannel = make(map[string]chan float64)
	o.SSEHandlers = make(map[string]*SSEHandler)
	o.Mutex = sync.RWMutex{}
}

func (o *OutputStorage) ReadOutput(channel chan OutputBundle) {
	go func() {
		for request := range channel {
			key := request.Output.RigId
			o.Mutex.Lock()
			if o.Position[key] == 0 || o.ReadPosition[key] >= o.Position[key] { // Return Empty Value to client
				log.Printf("In function ReadOutput\nBuffer for ID %s is empty\nSending empty JSON back to client\n", key)
				o.Mutex.Unlock()
				request.Channel <- OutputRequest{RigId: request.Output.RigId, Value: "udef"}
			} else { // Read value from storage and pass it to client
				log.Printf("In function ReadOutput\nBuffer for ID %s\n Sending value to client\n", key)
				request.Channel <- o.Output[key][o.ReadPosition[key]]
				o.ReadPosition[key]++
				o.Mutex.Unlock() // Unlock Mutex
			}
		}
	}()

}

func (o *OutputStorage) WriteOutput(channel chan OutputRequest) {
	go func() {
		for request := range channel {
			key := request.RigId
			o.Mutex.Lock()
			if handler, ok := o.SSEHandlers[key]; ok {
				log.Printf("Writing to existing SSEHandler\n")
				handler.EventInput <- []byte(request.Value)
			} else {
				log.Printf("SSEHandler not present during message write\n")
				sseHandler := &SSEHandler{}
				sseHandler.Init(key)
				go sseHandler.Listen()
				sseHandler.EventInput <- []byte(request.Value)
				o.SSEHandlers[key] = sseHandler
			}
			o.Mutex.Unlock()
		}
	}()
}

//-----------------------------STATS Functions-----------------------------

func (s *StatsStorage) Init() {
	s.Stats = make(map[string]StatsRequest)
	s.RetentionCounter = make(map[string]int)
	s.Mutex = sync.RWMutex{}
}

func (s *StatsStorage) WriteStats(channel chan StatsRequest) {
	go func() {
		for request := range channel {
			key := request.RigId
			log.Printf("In function WriteStats\nStat added to %s\n", key)
			s.Mutex.Lock()
			s.Stats[key] = request
			if s.RetentionCounter[key] == 0 {
				go FeedDB(key, request.Overall)
			}
			s.RetentionCounter[key]++
			if s.RetentionCounter[key] == 30 {
				s.RetentionCounter[key] = 0
			}
			s.Mutex.Unlock()
		}
	}()
}

func (s *StatsStorage) ReadStats(channel chan StatsBundle) {
	go func() {
		for request := range channel {
			key := request.Stat.RigId
			s.Mutex.RLock()
			if _, ok := s.Stats[key]; ok {
				log.Printf("In function ReadStats\nReading stat for %s\nSending to client\n", key)
				s.Mutex.RUnlock()
				request.Channel <- s.Stats[key]
			} else {
				s.Mutex.RUnlock()
				log.Printf("In function ReadStats\nNo valid stats for %s\nSending empty stat to client\n", key)
				request.Channel <- StatsRequest{RigId: key}
			}
		}
	}()
}
