package server

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"sync"
	"time"
)

type AuthData struct {
	Auth  map[string]bool
	Mutex sync.Mutex
}

type AuthRequest struct {
	Email    string
	FluenKey string
	Channel  chan bool
}

func (a *AuthData) Init() {
	a.Auth = make(map[string]bool)
	a.Mutex = sync.Mutex{}
}

func (a *AuthData) AuthMe(channel chan AuthRequest) {
	go func() {
		for request := range channel {
			a.Mutex.Lock()
			if stored, ok := a.Auth[request.Email+request.FluenKey]; ok {
				a.Mutex.Unlock()
				request.Channel <- stored
				log.Printf("Authenticating User: %s Using stored data\n", request.Email)
			} else {
				validUser, validResponse := AskAdmin(request.Email, request.FluenKey)
				if validResponse {
					a.Auth[request.Email+request.FluenKey] = validUser
				}
				a.Mutex.Unlock()
				request.Channel <- validUser
				log.Printf("Authenticating User: %s Using global data\n", request.Email)
			}
		}
	}()
}

func AskAdmin(email string, fluen string) (bool, bool) {
	official := false
	valid := false

	client := http.Client{Timeout: time.Duration(999 * time.Millisecond)}
	resp, err := client.Get(fmt.Sprintf("http://%s/api/sex/authsex?email=%s&fluen=%s", os.Getenv("SexAdminAddr"), email, fluen))
	if err != nil {
		log.Printf("Error connecting to Auth ENDPOINT\n%s", err.Error())
		return official, valid
	}

	status := resp.StatusCode
	resp.Body.Close()

	if status == 200 {
		official = true
		valid = true
		log.Printf("Auth of User: %s SUCCESSFUL\n", email)
	} else if status == 403 {
		valid = true
	}

	return official, valid
}

func InlineAuth(email string, fluen string, channel chan AuthRequest) bool {
	log.Printf("Creating auth Request for User: %s, Fluen: %s\n", email, fluen)
	if email == "" || fluen == "" {
		log.Printf("Errorin InlineAuth!\nEmpty creditnails\n")
		return false
	}
	resp := make(chan bool)
	channel <- AuthRequest{Email: email, FluenKey: fluen, Channel: resp}
	return <-resp
}
