package server

import (
	"log"
	"sync"
)

type SSEHandler struct {
	Clients        map[chan []byte]bool
	EventInput     chan []byte
	AddClient      chan chan []byte
	RemoveClient   chan chan []byte
	TimeoutChannel chan bool
	ClientAmount   int
	Mutex          sync.Mutex
}

func (s *SSEHandler) Init(key string) {
	s.Clients = make(map[chan []byte]bool)
	s.EventInput = make(chan []byte, 1000)
	s.AddClient = make(chan chan []byte)
	s.RemoveClient = make(chan chan []byte)
	s.TimeoutChannel = make(chan bool)
	s.ClientAmount = 0
	s.Mutex = sync.Mutex{}
	go TimeoutOutput(key, s.TimeoutChannel)
	log.Printf("SSEHandler initialized\n")
}

func (s *SSEHandler) Listen() {
	for {
		select {
		case client := <-s.AddClient:
			s.Mutex.Lock()
			s.Clients[client] = true
			s.ClientAmount++
			if s.ClientAmount == 1 {
				s.TimeoutChannel <- true
			}
			s.Mutex.Unlock()
			log.Printf("Client added to Message Broadcaster\n")

		case client := <-s.RemoveClient:
			s.Mutex.Lock()
			delete(s.Clients, client)
			s.ClientAmount--
			if s.ClientAmount == 0 {
				s.TimeoutChannel <- false
			}
			s.Mutex.Unlock()

		case message := <-s.EventInput:
			i := 0
			s.Mutex.Lock()
			for channel, _ := range s.Clients {
				channel <- message
				i++
			}
			s.Mutex.Unlock()
			log.Printf("Message %s broadcasted to %d clients\n", string(message), i)
		}
	}
}
