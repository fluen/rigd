package server

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
)

type OutputRequest struct {
	RigId string
	Value string
	Line  int
	Email string
	Fluen string
}

func PostWriteOutputHandler(channel chan OutputRequest, auth chan AuthRequest) http.HandlerFunc {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		decoder := json.NewDecoder(req.Body)
		Output := OutputRequest{}

		if err := decoder.Decode(&Output); err != nil {
			log.Printf("Error during json Decode in Func PostWriteOutputHandler!\nError: %s\nContact Server Administrator!\n", err.Error())
			rw.WriteHeader(500)
			rw.Write([]byte("ERROR"))
			return
		}

		if !InlineAuth(Output.Email, Output.Fluen, auth) {
			log.Printf("User: %s authentication failed\n", Output.Email)
			return
		}

		log.Printf("Message received from Rig: %s\nMessage: %s\n", Output.RigId, Output.Value)
		channel <- Output

		rw.WriteHeader(200)
		rw.Write([]byte("OK"))
		return
	})
}

func PostReadOutputHandler(channel chan OutputBundle, data *OutputStorage, auth chan AuthRequest) http.HandlerFunc {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		query := req.URL.Query()
		Output := OutputRequest{RigId: query.Get("RigId"),
			Email: query.Get("Email"),
			Fluen: query.Get("Fluen"),
		}

		if !InlineAuth(Output.Email, Output.Fluen, auth) {
			log.Printf("User: %s authentication failed\n", Output.Email)
			return
		}

		flusher, ok := rw.(http.Flusher)
		if !ok {
			log.Printf("Streaming not supported\n")
			return
		}

		log.Printf("Request received to read output from Rig: %s\nLine: %d\n", Output.RigId, Output.Line)

		data.Mutex.Lock()

		rw.Header().Set("Content-Type", "text/event-stream")
		rw.Header().Set("Cache-Control", "no-cache")
		rw.Header().Set("Connection", "keep-alive")
		rw.Header().Set("Access-Control-Allow-Origin", "*")

		log.Printf("Establishing SSEHandler\n")
		messageChan := make(chan []byte)
		var sseHandler *SSEHandler

		if handler, ok := data.SSEHandlers[Output.RigId]; ok {
			log.Printf("Using existing SSEHandler\n")
			sseHandler = handler
		} else {
			log.Printf("Starting SSEHandler creation\n")
			sseHandler = &SSEHandler{}
			sseHandler.Init(Output.RigId)
			go sseHandler.Listen()
			data.SSEHandlers[Output.RigId] = sseHandler
		}
		data.Mutex.Unlock()

		sseHandler.AddClient <- messageChan
		defer func() {
			sseHandler.RemoveClient <- messageChan
		}()

		notify := rw.(http.CloseNotifier).CloseNotify()
		go func() {
			<-notify
			sseHandler.RemoveClient <- messageChan
		}()

		for {
			msg := <-messageChan
			log.Printf("Message broadcasted to client: %s\n", msg)
			fmt.Fprintf(rw, "data: %s\n\n", msg)
			flusher.Flush()
		}
		return
	})
}

func TimeoutOutput(key string, channel chan bool) {
	for {
		select {
		case start := <-channel:
			if start {
				sendJSON("start", key)
				log.Printf("Opening connection for Id: %s\n", key)
			} else {
				sendJSON("stop", key)
				log.Printf("Closing connection for Id: %s\n", key)
			}
		}
	}
}

func sendJSON(action string, key string) {
	url := fmt.Sprintf("http://%s/api/sex/%soutput/%s", os.Getenv("SexAdminAddr"), action, key)
	resp, err := http.Get(url)
	if err != nil {
		log.Printf("Error during sending JSON to Admin for Id: %s\n%s", key, err.Error())
		return
	}
	resp.Body.Close()
}
