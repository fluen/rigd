package rig

import (
	"encoding/json"
	"errors"
	"log"
	"strconv"
	"strings"

	"gitlab.com/fluen/rigd/messages"
	"os"
)

func (r *Rig) Execute(msgJson []byte) error {

	var msg messages.GenericControlMsg

	if err := json.Unmarshal(msgJson, &msg); err != nil {
		log.Println("Cannot unmarshall msg: ", err)
		return err
	}

	log.Println(msg)
	if msg.RigId != "-1" && strings.Replace(msg.RigId, "-", "", -1) != r.Ident {
		log.Println("This msg is not for me. My id, msg id:", r.Ident, msg.RigId)
		return errors.New("This msg is not for me.")
	}

	switch msg.Type {

	case "FanSpeed":
		speed, err := strconv.ParseInt(msg.Value, 10, 32)
		if err != nil {
			log.Println("FanSpeed - cannot convert Value to int")
			return err
		}
		r.SetFanSpeed(msg.Pci, int(speed))

	case "PowerLimit":
		limit, err := strconv.ParseInt(msg.Value, 10, 32)
		if err != nil {
			log.Println("PowerLimit - cannot convert Value to int")
			return err
		}
		r.SetPowerLimit(msg.Pci, int(limit))

	case "MemOffset":
		offset, err := strconv.ParseInt(msg.Value, 10, 32)
		if err != nil {
			log.Println("MemOffset - cannot convert Value to int")
			return err
		}
		r.SetMemOffset(msg.Pci, int(offset))

	case "Run":
		switch msg.Value {
		case "claymore":
			r.StartClaymore()
		default:
			log.Println("I don't know this miner, yet: ", msg.Value)
		}

	case "Stop":
		if r.Miner != nil {
			r.Miner.Stop()
		}
		r.Miner = nil

	case "Sex":
		switch msg.Value {
		case "start":
			if r.Miner != nil {
				r.Miner.SetOutput(true)
			}
		case "stop":
			if r.Miner != nil {
				r.Miner.SetOutput(false)
			}
		}
	case "Restart":
		r.Restart()

	case "Update":
		r.VersionUpdate()
	case "Name":
		os.Exit(0)
	case "Flash":
		r.FlashHandler(msg.Pci, msg)
	}

	return nil

}
