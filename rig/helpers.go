package rig

import (
	"crypto/md5"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"

	"gitlab.com/fluen/rigd/messages"
)

var privKeyFile string = "/etc/fluen/key.priv"
var pubKeyFile string = "/etc/fluen/key.pub"
var identFile string = "/etc/fluen/ident"
var configFile string = "/etc/fluen/fluen.conf"
var versionFile string = "/etc/fluen/version"
var userProfileFile string = ""

type Config struct {
	FluenURL      string
	SexyServerURL string
	RabbitURL     string
	FluenPort     string
	Wallet        string
	Pool          string
	Email         string
	FluenKey      string
	RigName       string
	Unregistered  bool
}

func ReadConfig(file string) Config {
	var config Config
	if file == "" {
		file = configFile
	}
	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		fmt.Println(err.Error())
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)
	defer configFile.Close()
	return config
}

func SaveConfig(conf Config) error {
	configFile, err := os.Create(configFile)
	if err != nil {
		log.Println("Cannot open configFile for Writing")
		return err
	}
	data, err := json.Marshal(conf)
	if err != nil {
		log.Println("Cannot Marshal config file in SaveConfig")
		return err
	}
	_, err = configFile.Write(data)
	if err != nil {
		log.Println("Cannot save configFile", err)
		return err
	}
	defer configFile.Close()

	return nil
}

func ueventToMap(data string) map[string]string {

	entries := make(map[string]string)

	lines := strings.Split(data, "\n")
	for _, line := range lines {
		if line == "" {
			continue
		}
		entry := strings.Split(line, "=")
		entries[entry[0]] = entry[1]
	}

	return entries
}

func pubKeyToString(privatekey *rsa.PrivateKey) string {
	pubASN1, _ := x509.MarshalPKIXPublicKey(&privatekey.PublicKey)

	pubBytes := pem.EncodeToMemory(&pem.Block{
		Type:  "RSA PUBLIC KEY",
		Bytes: pubASN1,
	})

	return string(pubBytes[:])
}

func privKeyToString(privatekey *rsa.PrivateKey) string {
	pemdata := pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: x509.MarshalPKCS1PrivateKey(privatekey),
		},
	)

	return string(pemdata[:])

}

func (r *Rig) getIdent() string {

	ident := r.getIdentFromFile()

	log.Println("Ident:", ident)
	if ident != "" {
		return ident
	}

	currentTime := time.Now()
	t := md5.Sum([]byte(currentTime.String() + r.Name))
	ident = fmt.Sprintf("%x", string(t[:]))

	if e := ioutil.WriteFile(identFile, []byte(ident), 0644); e != nil {
		fmt.Println("Cannot write ident file to: ", identFile, e)
		return ""
	}

	return ident
}

func (r *Rig) getIdentFromFile() string {
	dat, err := ioutil.ReadFile(identFile)
	if err != nil {
		log.Println("Error reading rig ident: ", err)
		return ""
	}

	return string(dat[:])
}

func ReadVersion() (string, error) {

	dat, err := ioutil.ReadFile(versionFile)
	if err != nil {
		log.Println("Cannot read version from: ", versionFile)
		return "", err
	}
	return strings.TrimSpace(string(dat[:])), nil
}

func (r *Rig) getRSAKeys() (*rsa.PrivateKey, error) {
	pk, pkErr := messages.ReadKeyFromFile()
	if pkErr != nil {
		pk, pkErr = r.generateRSAKeys()
	}
	return pk, pkErr
}

func (r *Rig) generateRSAKeys() (*rsa.PrivateKey, error) {
	privatekey, err := rsa.GenerateKey(rand.Reader, 1024)
	if err != nil {
		fmt.Println("Cannot generate SHA keys: ", err.Error())
		return nil, err
	}

	if e := ioutil.WriteFile(privKeyFile, []byte(privKeyToString(privatekey)), 0644); e != nil {
		fmt.Println("Cannot save private key to: ", privKeyFile, e)
		return nil, e
	}
	if e := ioutil.WriteFile(pubKeyFile, []byte(pubKeyToString(privatekey)), 0644); e != nil {
		fmt.Println("Cannot save public key to: ", pubKeyFile, e)
		return nil, e
	}

	return privatekey, nil

}
