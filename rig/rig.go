package rig

import (
	"bufio"
	"bytes"
	"crypto/rsa"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"gitlab.com/fluen/rigd/gpudevices"
	"gitlab.com/fluen/rigd/miners"
	"gitlab.com/fluen/rigd/restarter"
)

// Rig is a main struct describing the whole mining rig.
// Rig is used as an entrypoint to execute all operations to be performed on a mining rig.
type Rig struct {
	GpuDevices []gpudevices.GPUDevice
	Name       string
	Ident      string
	PrivateKey *rsa.PrivateKey
	Miner      miners.Miner
	Config     Config
	Version    string
	Restarter  *restarter.Restarter
}

// NewRig is a constructor for Rig.
// NewRig also creates Identifier for a mining rig (in /etc/fluen/ident) and RSA Keys (stored in /etc/fluen/key.(priv|pub))
// This construcotor takes care of populating GpuDevices with correct entries.
func NewRig(config Config) *Rig {

	version, err := ReadVersion()
	if err != nil {
		os.Exit(0)
	}

	r := &Rig{
		Name:    config.RigName,
		Config:  config,
		Version: version,
	}

	restarter := restarter.Restarter{}
	restarter.Init()
	r.Restarter = &restarter

	r.Ident = r.getIdent()
	r.Miner = nil

	pk, pkErr := r.getRSAKeys()
	if pkErr != nil {
		log.Println(pkErr)
		return nil
	}

	r.PrivateKey = pk

	count, cErr := r.CountGPUDevices()
	if cErr != nil {
		log.Println(cErr)
		return nil
	}

	g, e := r.ListGPUDevices(count)
	if e != nil {
		log.Println(e)
		return nil
	}

	r.GpuDevices = g
	if r.Config.Unregistered != true {
		if e := r.sendVersion(); e != nil {
			os.Exit(0)
		}
	}
	return r
}

func (r *Rig) PingAlive() {
	go func() {
		endpoint := fmt.Sprintf("%s:%s/api/miner/pingalive?rigid=%s", r.Config.FluenURL, r.Config.FluenPort, r.Ident)
		for {
			r.sendGet(endpoint)
			time.Sleep(time.Second * 2)
		}
	}()
}

// ListGPUDevices looks into /sys/class/drm/card[...] in order to get a list of GPU Devices from a system.
// GPU Devices are usually graphic cards (Nvidia and AMD), so the list of GPU Devices is based on DRIVER section of an uevent
func (r *Rig) ListGPUDevices(count int) ([]gpudevices.GPUDevice, error) {

	// /sys/class/drm/card3/device/uevent
	log.Println("ListGPUDevices, count:", count)
	var devices []gpudevices.GPUDevice

	for i := 0; i < count; i++ {
		file := fmt.Sprintf("/sys/class/drm/card%d/device/uevent", i)
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Println("Cannot read file: ", file, err)
			return nil, err
		}
		entries := ueventToMap(string(data))

		dc := gpudevices.GPUDeviceCommon{
			Driver: entries["DRIVER"],
			Pci:    entries["PCI_SLOT_NAME"],
			Uid:    i,
		}

		supportedDrivers := []string{"nvidia", "amdgpu"}
		supported := false
		for _, v := range supportedDrivers {
			if entries["DRIVER"] == v {
				supported = true
				break
			}
		}

		if !supported {
			continue
		}

		// Check device model
		file = fmt.Sprintf("/sys/class/drm/card%d/device/device", i)
		data, err = ioutil.ReadFile(file)
		if err != nil {
			log.Println("Cannot read file: ", file, err)
			return nil, err
		}

		pcids, e := os.Open("/usr/share/misc/pci.ids")
		if e != nil {
			log.Println(e)
			return nil, err
		}
		scanner := bufio.NewScanner(pcids)

		for scanner.Scan() {
			if strings.Contains(scanner.Text(), strings.TrimSpace(strings.Replace(string(data[:]), "0x", "", -1))) {
				dc.Model = scanner.Text()
				log.Println(dc.Model)
				break
			}
		}

		var dev gpudevices.GPUDevice
		switch entries["DRIVER"] {
		case "nvidia":
			dev = gpudevices.NewNvidia(&dc)
			if dev == nil {
				log.Fatalf("Cannot create Rig. NewNvidia() failed.")
			}
		case "amdgpu":
			dev = &gpudevices.Amd{&dc}
		}

		devices = append(devices, dev)
	}

	return devices, nil

}

// CountGPUDevices is used to return a number of VGA devices avaliable on a rig.
// It uses "lspci" command to determine the count. And yes. It's not perfect and beautiful...
func (r *Rig) CountGPUDevices() (int, error) {
	cmd := "lspci |grep VGA |wc -l"
	out, err := exec.Command("bash", "-c", cmd).Output()
	if err != nil {
		log.Println("DetectGPUDevices error")
		return 0, err
	}
	s := strings.TrimSpace(string(out))

	c, e := strconv.ParseInt(s, 10, 32)
	if e != nil {
		log.Println("Cannot convert to int: ", e)
		return 0, e
	}

	return int(c), nil

}

// GetGPUDevice return a single gpudevices.GPUDevice object from a list of devices attached to rig, based on given uid
func (r *Rig) GetGPUDevice(pci string) (gpudevices.GPUDevice, error) {

	for _, d := range r.GpuDevices {
		if d.GetDeviceCommon().Pci == pci {
			return d, nil
		}
	}

	return nil, errors.New("Provided pci is not correct")
}

func (r *Rig) SetMiner(miner string) error {
	switch miner {
	case "claymore":
		r.Miner = miners.NewClaymore(r.Config.Wallet, r.Config.Pool, r.Config.SexyServerURL, r.GpuDevices, r.Config.Email, r.Config.FluenKey, r.Restarter)
	default:
		return errors.New("Provided miner is not supported. Or maybe a wrong name?")
	}

	return nil
}

func (r *Rig) StartClaymore() {
	r.SetMiner("claymore")
	go r.Miner.RunInShell(r.Ident, r.Name)
	go r.Miner.ReadStats(r.Ident)
}

func (r *Rig) SetProfile() error {
	log.Println("SETPROFILE")
	endpoint := fmt.Sprintf("%s:%s/api/miner/rigprofile?rigId=%s&email=%s", r.Config.FluenURL, r.Config.FluenPort, r.Ident, r.Config.Email)
	params, err := r.sendGet(endpoint)
	log.Println("PARAMS:", string(params[:]))
	if err != nil {
		log.Println("SetProfile error: ", err)
		return err
	}

	type ProfileDump struct {
		Name        string
		PoolName    string
		PoolPort    string
		WalletValue string
	}

	profile := ProfileDump{}

	if err := json.Unmarshal(params, &profile); err != nil {
		log.Println("Unmarshaling ProfileDump failed: ", err)
		return err
	}

	log.Println(profile)
	if profile.WalletValue == "" {
		return errors.New("Empty Wallet. Try again later.")
	}

	r.Config.Wallet = profile.WalletValue
	r.Config.Pool = fmt.Sprintf("%s:%s", profile.PoolName, profile.PoolPort)

	SaveConfig(r.Config)
	return nil

}

func (r *Rig) SetParams() error {

	type GpuParam struct {
		Name       string
		Pci        string
		FanSpeed   int
		MemOffset  int
		PowerLimit int
		Voltage    float32
		Flashed    bool
	}

	endpoint := fmt.Sprintf("%s:%s/api/miner/startupparams?rigid=%s", r.Config.FluenURL, r.Config.FluenPort, r.Ident)
	params, err := r.sendGet(endpoint)
	if err != nil {
		log.Println("SetParams error: ", err)
		return err
	}

	gpuParams := []GpuParam{}

	if err := json.Unmarshal(params, &gpuParams); err != nil {
		log.Println("Unmarshaling Params failed: ", err)
		return err
	}

	for _, gpu := range gpuParams {
		r.Name = gpu.Name
		d, e := r.GetGPUDevice(gpu.Pci)
		if e != nil {
			log.Println("SetParams: Cannot find GPU Device ID: ", gpu.Pci, e)
			continue
		}
		if !gpu.Flashed {
			d.SetFanSpeed(gpu.FanSpeed)
			d.SetPowerLimit(gpu.PowerLimit)
			d.SetMemTROffset(gpu.MemOffset)
		}
	}

	mapper := func(r rune) rune {
		switch {
		case r >= 'A' && r <= 'Z':
			return r
		case r >= 'a' && r <= 'z':
			return r
		case r >= '0' && r <= '9':
			return r

		}
		return rune('_')
	}

	r.Name = strings.Map(mapper, r.Name)

	return nil
}

func (r *Rig) VersionUpdate() error {
	//	http://localhost:8087/update
	endpoint := "http://localhost:8087/update"
	req, err := http.NewRequest("GET", endpoint, nil)
	req.Header.Set("version", r.Version)

	client := &http.Client{}
	resp, err := client.Do(req)
	log.Println("VersionUpdate executed")
	if err != nil {
		log.Println("sendGet error: ", err)
		return err
	}

	defer resp.Body.Close()

	return nil

}

func (r *Rig) sendVersion() error {
	//	endpoint := "/api/miner/setversion?version=VER"

	endpoint := fmt.Sprintf("%s:%s/api/miner/setversion?version=%s", r.Config.FluenURL, r.Config.FluenPort, r.Version)
	_, err := r.sendGet(endpoint)
	if err != nil {
		log.Println("sendVersion() error: ", err)
		return err
	}

	return nil

}

func (r *Rig) sendGet(endpoint string) ([]byte, error) {
	req, err := http.NewRequest("GET", endpoint, nil)
	req.Header.Set("fluen-key", r.Config.FluenKey)
	req.Header.Set("email", r.Config.Email)
	req.Header.Set("rigid", r.Ident)

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		log.Println("sendGet error: ", err)
		return nil, err
	}

	defer resp.Body.Close()
	responseData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("Cannot read response from GET: ", err)
		return nil, err
	}

	if resp.StatusCode != 200 {
		log.Println("sendGet StatusCode: ", resp.Status)
		return nil, errors.New(resp.Status)
	}

	fmt.Println("response Status:", resp.Status)
	return responseData, nil
}

func (r *Rig) sendPost(endpoint string, msg interface{}) error {

	buf := new(bytes.Buffer)
	err := json.NewEncoder(buf).Encode(msg)
	if err != nil {
		return err
	}
	req, err := http.NewRequest("POST", endpoint, buf)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("fluen-key", r.Config.FluenKey)
	req.Header.Set("email", r.Config.Email)
	req.Header.Set("rigid", "-1")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return errors.New(resp.Status)
	}

	log.Println("response Status:", resp.Status)
	body, _ := ioutil.ReadAll(resp.Body)
	log.Println("response Body:", string(body))

	return nil

}
