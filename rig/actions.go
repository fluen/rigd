package rig

import (
	"fmt"
	"log"
	"os/exec"

	"net/http"

	"gitlab.com/fluen/rigd/messages"
)

// RegisterRig sends a POST call to Fluen API (/api/miner/register) in order to register a new rig or update existing rig in a system.
func (r *Rig) RegisterRig() error {

	rigMsg := messages.RigMsg{
		Ident:  r.Ident,
		Name:   r.Name,
		PubKey: pubKeyToString(r.PrivateKey),
	}

	for _, dev := range r.GpuDevices {
		devMsg := messages.GPUDeviceMsg{
			Pci:    dev.GetDeviceCommon().Pci,
			Model:  dev.GetDeviceCommon().Model,
			Uid:    dev.GetDeviceCommon().Uid,
			Driver: dev.GetDeviceCommon().Driver,
		}
		rigMsg.GPUDevices = append(rigMsg.GPUDevices, devMsg)
	}

	endpoint := fmt.Sprintf("%s:%s/api/miner/register", r.Config.FluenURL, r.Config.FluenPort)
	log.Println("Registering RIG at:", endpoint)
	if e := r.sendPost(endpoint, rigMsg); e != nil {
		log.Println("RegisterRig error: ", e)
		return e
	}

	return nil
}

// SetFanSpeed is a method that sets a fan speed parameter on specified GPU device.
// If card parameter is -1, fan speed will be set on all avaliable GPU devices.
func (r *Rig) SetFanSpeed(card string, speed int) error {

	if card != "-1" {
		dev, err := r.GetGPUDevice(card)
		if err != nil {
			return err
		}
		if err := dev.SetFanSpeed(speed); err != nil {
			return err
		}
		return nil
	}

	for _, c := range r.GpuDevices {
		if err := c.SetFanSpeed(speed); err != nil {
			return err
		}
	}

	return nil

}

// SetPowerLimit is a method that sets a power limit parameter on specified GPU device.
// If card parameter is -1, power limit  will be set on all avaliable GPU devices.
func (r *Rig) SetPowerLimit(card string, limit int) error {
	if card != "-1" {
		dev, err := r.GetGPUDevice(card)
		if err != nil {
			return err
		}
		if err := dev.SetPowerLimit(limit); err != nil {
			return err
		}
		return nil
	}

	for _, c := range r.GpuDevices {
		if err := c.SetPowerLimit(limit); err != nil {
			return err
		}
	}

	return nil

}

// SetMemOffset is a method that sets a memory offset parameter on specified GPU device.
// If card parameter is -1, memory offset will be set on all avaliable GPU devices.
func (r *Rig) SetMemOffset(card string, offset int) error {

	if card != "-1" {
		dev, err := r.GetGPUDevice(card)
		if err != nil {
			return err
		}
		if err := dev.SetMemTROffset(offset); err != nil {
			return err
		}
		return nil
	}

	for _, c := range r.GpuDevices {
		if err := c.SetMemTROffset(offset); err != nil {
			return err
		}
	}

	return nil

}

func (r *Rig) Restart() {
	// Maybe this is not the best idea... but it should work every time.
	cmd := exec.Command("/bin/bash", "-c", "echo b > /proc/sysrq-trigger")
	cmd.Run()

	url := "http://localhost:8087/restart"

	req, e := http.NewRequest("GET", url, nil)
	if e != nil {
		return
	}
	res, e2 := http.DefaultClient.Do(req)
	if e2 != nil {
		return
	}
	defer res.Body.Close()
}
