package rig

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/fluen/rigd/messages"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"
)

//./atiflash -i | sed 's/  */ /g' | cut -d ' ' -f 2,3 | grep " 03" | cut -d ' ' -f 1

var atiFlashPath string = "/opt/fluen/atiflash"
var atiFlashShortPath string = "atiflash"

type Event struct {
	Type  string
	Name  string
	Value string
}

func (r *Rig) FlashHandler(pci string, bios messages.GenericControlMsg) {
	biosName, err := GetBios(bios.Value)

	if err != nil {
		go r.SendEvent(Event{Type: "error", Name: "flash", Value: fmt.Sprintf("%s %s", "Error downloading bios file", err.Error())})
		log.Printf("Error downloading bios\n%s\nAborting!!!\n", err.Error())
		return
	}

	deviceAdapter, err := GetDeviceAdapter(pci)
	if err != nil {
		go r.SendEvent(Event{Type: "error", Name: "flash", Value: fmt.Sprintf("%s %s", "Error getting device adapter", err.Error())})
		log.Printf("Error getting adapter id\n%s\nAborting!!!", err.Error())
		return
	}

	err = Flash(r, deviceAdapter, biosName)
	if err != nil {
		log.Printf("Error during flash\n%s\nAborting!!!", err.Error())
		return
	}

	go r.SendEvent(Event{Type: "success", Name: "flash", Value: fmt.Sprintf("%s %s", "Successfuly flashed BIOS ", biosName)})
	time.Sleep(5 * time.Second)

	err = Reboot()
	if err != nil {
		go r.SendEvent(Event{Type: "error", Name: "flash", Value: fmt.Sprintf("%s %s", "Error rebooting rig", err.Error())})
		log.Printf("Error during reboot\n%s\nAborting!!!", err.Error())
		return
	}
}

func GetDeviceAdapter(pci string) (string, error) {
	bn := strings.TrimPrefix(strings.TrimSuffix(pci, ":00.0"), "0000:")

	cmd := exec.Command("/bin/bash", "-c", fmt.Sprintf(`%s/atiflash -i | sed 's/  */ /g' | cut -d ' ' -f 2,3 | grep ' %s' | cut -d ' ' -f 1`, atiFlashPath, bn))
	cOut, err := cmd.CombinedOutput()
	if err != nil {
		return "", err
	}
	stringOutput := string(cOut[:])

	return strings.TrimSuffix(stringOutput, "\n"), nil
}

func Flash(r *Rig, adapter string, biosName string) error {
	command := fmt.Sprintf(`%s/atiflash -p %s %s/%s`, atiFlashPath, adapter, atiFlashPath, biosName)
	log.Printf("%s\n", command)
	cmd := exec.Command("/bin/bash", "-c", command)
	//cmd := exec.Command(fmt.Sprintf("%s/atiflash", atiFlashPath), "-p", adapter, fmt.Sprintf("%s/%s", atiFlashShortPath, biosName))
	out, err := cmd.CombinedOutput()
	log.Printf("%s\n", string(out[:]))
	if err != nil {
		go r.SendEvent(Event{Type: "error", Name: "flash", Value: fmt.Sprintf("%s %s", "Error flashing bios", string(out[:]))})
		return err
	}

	return nil
}

func Reboot() error {
	cmd := exec.Command("reboot")
	err := cmd.Run()
	if err != nil {
		return err
	}
	return nil
}

func GetBios(link string) (string, error) {
	fileNameSplit := strings.Split(link, "/")
	fileName := fileNameSplit[len(fileNameSplit)-1]
	file, err := os.Create(fmt.Sprintf("%s/%s", atiFlashPath, fileName))
	defer file.Close()

	if err != nil {
		log.Printf("Error creating update file\n%s\n", err.Error())
		return "", errors.New(fmt.Sprintf("Error creating bios file\n%s\n", err.Error()))
	}
	defer file.Close()

	toDownload := fmt.Sprintf("%s", link)
	log.Println("I'm about to download: ", toDownload)
	resp, err := http.Get(toDownload)
	if err != nil {
		log.Printf("Error downloading bios file\n%s\n", err.Error())
		return "", errors.New(fmt.Sprintf("Error downloading bios file\n%s\n", err.Error()))
	}
	if resp.StatusCode != 200 {
		log.Printf("Error downloading bios file\n")
		return "", errors.New(fmt.Sprintf("Error downloading bios file\n%s\n", resp.Status))
	}
	defer resp.Body.Close()

	_, err = io.Copy(file, resp.Body)
	if err != nil {
		log.Printf("Error saving bios file\n%s\n", err.Error())
		return "", errors.New(fmt.Sprintf("Error saving bios file\n%s\n", err.Error()))
	}

	log.Printf("Bios file saved successfuly\n")
	return fileName, nil
}

func (r *Rig) SendEvent(event Event) {
	url := fmt.Sprintf("%s:%s/api/miner/addevent", r.Config.FluenURL, r.Config.FluenPort)
	jsonEvent, err := json.Marshal(&event)
	if err != nil {
		log.Printf("Error during json.Marshal\n%s", err.Error())
		return
	}

	payload := strings.NewReader(string(jsonEvent))

	req, err := http.NewRequest("POST", url, payload)

	if err != nil {
		log.Printf("Error creating http request\n%s", err.Error())
		return
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("fluen-key", r.Config.FluenKey)
	req.Header.Add("email", r.Config.Email)
	req.Header.Add("rigid", r.Ident)

	res, err := http.DefaultClient.Do(req)

	if err != nil {
		log.Printf("Error doing request\n%s", err.Error())
		return
	}

	defer res.Body.Close()
	return
}
